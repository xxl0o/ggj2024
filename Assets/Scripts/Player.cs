using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    public enum JokeType : int
    {
        JOKE_TYPE_1 = 0,
        JOKE_TYPE_2,
        JOKE_TYPE_3,
        JOKE_TYPE_4,
        JOKE_TYPE_5,
        JOKE_TYPE_6,
        JOKE_TYPE_7,
        JOKE_TYPE_8,
        JOKE_TYPE_MAX
    }
    
    public static float[] jokesVolatility = //LOOK UP FOR JokeType
    {
        2,
        2,
        2,
        2,
        5,
        5,
        5,
        5
    };
    public static int JOKES_AMOUNT_IN_STACK = 4;
    public JokeType[] jokesPool;
    public bool IsJokesReady;
    public void MakeJokes()
    { 
        for(int i = 0; i < JOKES_AMOUNT_IN_STACK; i++)
        {
            JokeType generatedJoke = (JokeType)UnityEngine.Random.Range((int)JokeType.JOKE_TYPE_1, (int)JokeType.JOKE_TYPE_8);
            while(Array.Exists(jokesPool, p => p == generatedJoke))
            {
                generatedJoke = (JokeType)UnityEngine.Random.Range((int)JokeType.JOKE_TYPE_1, (int)JokeType.JOKE_TYPE_8);
            }
            jokesPool[i] = generatedJoke;
        }
    }
    void Start()
    {
        jokesPool = new JokeType[JOKES_AMOUNT_IN_STACK];
    }

    public GameObject projectile;
    public float shootForceForward = 20.0f;
    public float shootForceUp = 3.5f;
    public float shootForceRandom = 1.5f;

    void Update()
    {
        
    }

    void OnFire(InputValue value)
    {
        if (!value.isPressed)
        {
            return;
        }

        var offset = transform.forward * 0.5f - transform.right * 0.2f;
        var force = transform.forward * shootForceForward + transform.up * shootForceUp + UnityEngine.Random.insideUnitSphere * shootForceRandom;

        GameObject proj = Instantiate(projectile, transform.position + offset, transform.rotation);
        proj.GetComponent<Rigidbody>().velocity = force;
    }
}
