using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System;

public class Game : MonoBehaviour
{
    enum State
    {
        STATE_MENU = 0,
        STATE_STARTED,
        STATE_PAUSED
    }
    enum GameplayState
    {
        INGAME_STATE_JOKE_AVAILABLE = 0,
        INGAME_STATE_JOKE_PROCEEDING,
        INGAME_STATE_JOKE_REACTION
    }
    public enum JokeType : int
    {
        JOKE_TYPE_1 = 0,
        JOKE_TYPE_2,
        JOKE_TYPE_3,
        JOKE_TYPE_4,
        JOKE_TYPE_5,
        JOKE_TYPE_6,
        JOKE_TYPE_7,
        JOKE_TYPE_8,
        JOKE_TYPE_MAX
    }

    public static float[] jokesVolatility = //LOOK UP FOR JokeType
    {
        2,
        2,
        2,
        2,
        5,
        5,
        5,
        5
    };

    State gameState;
    GameplayState gameplayState;
    public static int JOKES_AMOUNT_IN_STACK = 4;
    public JokeType[] jokesPool;
    public static Spectator[] spectators;
    public bool IsJokesReady;
    public GameObject projectile;
    public float shootForceForward = 20.0f;
    public float shootForceUp = 3.5f;
    public float shootForceRandom = 1.5f;
    public void MakeJokes()
    {
        for (int i = 0; i < JOKES_AMOUNT_IN_STACK; i++)
        {
            JokeType generatedJoke = (JokeType)UnityEngine.Random.Range((int)JokeType.JOKE_TYPE_1, (int)JokeType.JOKE_TYPE_8);
            while (Array.Exists(jokesPool, p => p == generatedJoke))
            {
                generatedJoke = (JokeType)UnityEngine.Random.Range((int)JokeType.JOKE_TYPE_1, (int)JokeType.JOKE_TYPE_8);
            }
            jokesPool[i] = generatedJoke;
        }
    }
    void throwJoke(int index)
    {
        foreach (Spectator s in spectators)
        {
            s.TakeJoke(jokesPool[index]);
            s.IndicateMood();
        }
    }
    void Start()
    {
        gameState = State.STATE_MENU;
        gameplayState = GameplayState.INGAME_STATE_JOKE_AVAILABLE;
        IsJokesReady = false;
        jokesPool = new JokeType[JOKES_AMOUNT_IN_STACK];
        spectators = FindObjectsByType<Spectator>(FindObjectsSortMode.None);
    }
    void Update()
    {
        switch(gameState)
        {
            case State.STATE_MENU:
            {
                break;
            }
            case State.STATE_STARTED:
            {
                switch(gameplayState)
                {
                    case GameplayState.INGAME_STATE_JOKE_AVAILABLE:
                    {
                        if(IsJokesReady)
                        {
                            MakeJokes();
                            IsJokesReady = true;
                        }
                        break;
                    }
                    case GameplayState.INGAME_STATE_JOKE_PROCEEDING:
                    {
                        break;
                    }
                    case GameplayState.INGAME_STATE_JOKE_REACTION:
                    {
                        IsJokesReady = false;
                        gameplayState = GameplayState.INGAME_STATE_JOKE_AVAILABLE;
                        break;
                    }
                }
                break;
            }
            case State.STATE_PAUSED:
            {
                break;
            }
        }
    }
    void OnChooseJoke(InputValue value)
    {
        //if (gameState == State.STATE_STARTED)
        {
            if (value.Get().Equals(new Vector2(0, 1)))
            {
                throwJoke(0);
            }
            else if (value.Get().Equals(new Vector2(0, -1)))
            {
                throwJoke(1);
            }
            else if (value.Get().Equals(new Vector2(-1, 0)))
            {
                throwJoke(2);
            }
            else if (value.Get().Equals(new Vector2(1, 0)))
            {
                throwJoke(3);
            }
            gameplayState = GameplayState.INGAME_STATE_JOKE_REACTION; 
        } 
    }

    void OnFire(InputValue value)
    {
        if (!value.isPressed)
        {
            return;
        }

        var offset = transform.forward * 0.5f - transform.right * 0.2f;
        var force = transform.forward * shootForceForward + transform.up * shootForceUp + UnityEngine.Random.insideUnitSphere * shootForceRandom;

        GameObject proj = Instantiate(projectile, transform.position + offset, transform.rotation);
        proj.GetComponent<Rigidbody>().velocity = force;
    }
}
