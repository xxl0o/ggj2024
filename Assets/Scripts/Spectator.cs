using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spectator : MonoBehaviour
{
    [HideInInspector]
    public float mood;
    [HideInInspector]
    public float[] jokeTolerance;
    float hitDamage = 10.0f;
    float hitJoy = 2.0f;
    bool isAlive;
    [HideInInspector]
    public BoxCollider collider;
    float[] normiesRange = { 2, 4 };
    SpriteRenderer Indicator;
    public Sprite IndicatorSprite;
    private float offset = 2f; 
    private float maxMood = 20f; 
    void Start()
    {
        mood = 0;
        jokeTolerance = new float[(int)Player.JokeType.JOKE_TYPE_MAX];
        GenerateJokeTolerance();
        isAlive = true;
        collider = GetComponent<BoxCollider>();
        GameObject newObject = new GameObject("NewSpriteRenderer");
        Indicator = newObject.AddComponent<SpriteRenderer>();
        Indicator.sprite = IndicatorSprite;
        Indicator.color = Color.green;
        Indicator.transform.localScale = new Vector3(6, 6, 6);
        newObject.transform.SetParent(transform);
        Indicator.transform.position = new Vector3(transform.position.x, transform.position.y + offset, transform.position.z);
        HideMood();
    }
    public void IndicateMood()
    {
        Indicator.color = new Color(mood / maxMood - 0.5f, mood / maxMood + 0.5f, 0, 1);
    }
    public void HideMood()
    {
        Indicator.color = new Color();
    }
    void GenerateJokeTolerance()
    {
        for(int i = 0; i < (int)Game.JokeType.JOKE_TYPE_MAX; i++)
        {
            jokeTolerance[i] = Random.Range(normiesRange[0] - Game.jokesVolatility[0], normiesRange[1] + Game.jokesVolatility[1]);
        }
    }

    void AdjustMood(float delta)
    {
        mood = Mathf.Clamp(mood + delta, -maxMood, maxMood);
        if (mood <= -maxMood)
        {
            GetComponent<WalkAnimation>().Leave();
        }
        IndicateMood();
    }

    public void TakeJoke(Game.JokeType jokeType)
    {
        AdjustMood(jokeTolerance[(int)jokeType]);
    }
    public void TakeHit()
    {
        AdjustMood(-hitDamage);
    }
    public void SeeHit()
    {
        AdjustMood(+hitJoy);
    }
    void Update()
    {

    }
}
