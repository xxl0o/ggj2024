using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phone : MonoBehaviour
{
    [SerializeField] GameObject jokeTopic;
    [SerializeField] float initialPosition = -223.8f;
    [SerializeField] float jokeSpacing = 10f;

    // Start is called before the first frame update
    void Start()
    {
        var joke1 = GetJoke(0);
        joke1.transform.Find("Text").GetComponent<TMPro.TextMeshProUGUI>().text = "1. \uf004";
        var joke2 = GetJoke(1);
        joke2.transform.Find("Text").GetComponent<TMPro.TextMeshProUGUI>().text = "2. \uf0f5";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    GameObject GetJoke(int position) {
        GameObject joke = Instantiate(jokeTopic, Vector3.zero, Quaternion.identity, transform.Find("Canvas"));
        RectTransform rt = joke.GetComponent<RectTransform>();
        rt.anchoredPosition3D = new Vector3(0, initialPosition - position * jokeSpacing, 0);
        rt.localRotation = Quaternion.Euler(Vector3.zero);
        return joke;
    } 
}
