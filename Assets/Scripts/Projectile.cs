using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(transform.position, Vector3.zero) > 1000.0f)
        {
            // Gone out of game space
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            // Ignore player collisions
            return;
        }
        else if (collision.gameObject.tag == "Environment")
        {
            // Collision with something other than spectator (play miss sound?)
        }
        else if (collision.gameObject.tag == "Spectator")
        {
            var hit = collision.gameObject.GetComponent<Spectator>();
            hit.TakeHit();

            foreach (var spec in Game.spectators)
            {
                if (spec != hit)
                {
                    spec.SeeHit();
                }
            }
        }

        Destroy(gameObject);
    }
}
